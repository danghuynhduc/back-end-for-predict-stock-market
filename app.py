import time
from flask import Flask,request, url_for, redirect, render_template, jsonify
from pycaret.regression import *
import pandas as pd
import pickle
import numpy as np
import using_model_saved as ums

app = Flask(__name__)

@app.route('/')
def home():
    return {"notice": "welcome!!!"}

@app.route('/time')
def get_current_time():
    (predictPrice,realPrice,scoreTestMSE,scoreTestRMSE,date) = ums.using_model()
    return {
            'time': time.time(),
            'predictPrice':predictPrice.tolist()[0],
            'realPrice': realPrice.tolist()[0],
            'scoreTestMSE':scoreTestMSE,
            'scoreTestRMSE':scoreTestRMSE,
            'date':date.tolist()
            }

@app.route('/predictfuture',methods=['POST'])
def predictFuture():
    # data_unseen = pd.DataFrame([final], columns = cols)
    # prediction = predict_model(model, data=data_unseen, round = 0)
    # prediction = int(prediction.Label[0])
    # print("int_features: ",int_features)
    # print("final: ",final)
    data = request.get_json()
    print(data)
    print(data['open'])
    predictPrice = ums.predictFuture(float(data['open']),float(data['close']),float(data['volumn']))
    return {
            'predictPrice':predictPrice.tolist()[0],
            }

@app.route('/choosemodel',methods=['POST'])
def predict():
    # data_unseen = pd.DataFrame([final], columns = cols)
    # prediction = predict_model(model, data=data_unseen, round = 0)
    # prediction = int(prediction.Label[0])
    # print("int_features: ",int_features)
    # print("final: ",final)
    data = request.get_json()
    print(data)
    (predictPrice,realPrice,scoreTestMSE,scoreTestRMSE,date) = ums.using_model(data['model'])
    return {
            'time': time.time(),
            'predictPrice':predictPrice.tolist()[0],
            'realPrice': realPrice.tolist()[0],
            'scoreTestMSE':scoreTestMSE,
            'scoreTestRMSE':scoreTestRMSE,
            'date':date.tolist()
            }
    # return {'notice': data['model']}

@app.route('/predict_api',methods=['POST'])
def predict_api():
    data = request.get_json(force=True)
    data_unseen = pd.DataFrame([data])
    prediction = predict_model(model, data=data_unseen)
    output = prediction.Label[0]
    return jsonify(output)

if __name__ == '__main__':
    app.run(debug=True)
