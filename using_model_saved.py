from keras.models import load_model
import math
import pandas as pd
import numpy as np
from IPython.display import display

from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from keras.metrics import mean_squared_error
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import MinMaxScaler

# import visualize as vs
import stock_data as sd

sttModel=[  'ImprovedModelLSTM2_100epochs.h5',
            'basic_lstm_2epochs.h5',
            'basic_lstm_50epochs.h5',
            'ImprovedModelLSTM1_50epochs.h5',
            'ImprovedModelLSTM1_100epochs.h5',
            'ImprovedModelLSTM2_50epochs.h5',
            'ImprovedModelLSTM2_100epochs.h5',
            'ImprovedModelLSTM3_50epochs.h5',
            'ImprovedModelLSTM3_100epochs.h5',
            ]

def predictFuture(m,n,p):
    scaler = MinMaxScaler()
    scaler2 = MinMaxScaler()
    root_data = pd.read_csv('coca-cola.csv')
    numerical = ['open', 'close', 'volume']
    numeri = ['close']
    err  = scaler2.fit_transform(root_data[numeri])
    print("err",err)
    a = scaler.fit_transform(root_data[numerical])
    print("a",a)

    # test = np.array([[127.82, 130.48, 96906490]])
    test = np.array([[m, n, p]])
    print("test",test)

    # test.reshape(-1,1)
    test = scaler.transform(test)
    bk = []
    bk.append(test)
    bk = np.array(bk)

    print(bk)
    # bk = scaler.transform(bk)
    # print("bk",bk)
    model = load_model('testModel.h5')
    # model.summary()
    predictions = model.predict(bk)
    print("pre_test",predictions)
    predictions=scaler2.inverse_transform(predictions)
    print("pre_test",predictions)
    return predictions;

def using_model(num=0):
    model = load_model(sttModel[num])
    # model.summary()

    stocks = pd.read_csv('coca-cola_preprocessed.csv')
    root_data = pd.read_csv('coca-cola.csv')
    stocks_data = stocks.drop(['Item'], axis =1)
    date_preProcess = root_data
    display(stocks_data.head())

    X_train, X_test,y_train, y_test, date = sd.train_test_split_lstm(stocks_data,date_preProcess, 1, 1200)

    unroll_length = 50
    X_train = sd.unroll(X_train, unroll_length)
    X_test = sd.unroll(X_test, unroll_length)
    y_train = y_train[-X_train.shape[0]:]
    y_test = y_test[-X_test.shape[0]:]
    date = date[-X_test.shape[0]:]
    scaler = MinMaxScaler()
    numerical = ['close']
    scaler.fit_transform(root_data[numerical])
    predictions = model.predict(X_test).T
    predictions=scaler.inverse_transform(predictions)
    
    testScoreMSE = model.evaluate(X_test, y_test, verbose=0)
    testScoreRMSE = math.sqrt(testScoreMSE)
    y_test=scaler.inverse_transform([y_test])
    return (predictions,y_test,testScoreMSE,testScoreRMSE,date)
# a = np.array([[1],[2],[3]]).T.tolist()[0]
# b = [1,2,3]
# print(b)
# print(len(b))
# arr = np.array([1,2, 3, 4])
# ae = np.array([[1],[2],[4],[5]])

# print(arr)
# print(arr.tolist)

# print(ae)
# print(ae.tolist)
# print("Xtrain: ",X_train[0:5])
# print("ytrain: ",y_train[0:5])
# print("Xtest: ",X_test[0:5])
# print("predict : ",predictions[0:5])
# print("yTest: ",y_test[0:5])

# print(predictions[-5:])
# print(y_test[-5:])
# arr=[]
# for i in range(5,0,-1):
#     arr.append(100-abs(predictions[-i]-y_test[-i])*100/y_test[-i])
# print("H of array: ")
# for ar in arr:
#     print(ar)

# vs.plot_lstm_prediction(y_test,predictions,'Predict width improve lstm 15d')

# trainScore = model.evaluate(X_train, y_train, verbose=0)
# print('Train Score: %.8f MSE (%.8f RMSE)' % (trainScore, math.sqrt(trainScore)))

# testScore = model.evaluate(X_test, y_test, verbose=0)
# print('Test Score: %.8f MSE (%.8f RMSE)' % (testScore, math.sqrt(testScore)))